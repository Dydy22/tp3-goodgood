<?php
/*
Template Name: Equipe
Template Post Type: équipe
*/
?>
<section>
    <div class="container">
      <h2>Équipe</h2>
      <div class="row">
<?php
$equipeloop = new WP_Query(
    array(
        'post_type' => 'équipe',
        'posts_per_page' => 4
    )
);
while ( $equipeloop->have_posts() ) : $equipeloop->the_post();
?>
 <div class="col-lg-3">
          <div>
          <?php the_post_thumbnail('post-thumbnail', array('class' => 'img-round'))?>
          </div>
          <h3 class="centered"><?php the_title();?></h3>
        </div>
 
<?php endwhile;
?>

        
    <div class="line"></div>
</div>
</section>
