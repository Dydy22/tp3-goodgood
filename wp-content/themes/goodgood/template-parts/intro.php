<?php
/*
Template Name: Introduction
Template Post Type: intro
*/
?>
<section>
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <img src="<?php echo get_template_directory_uri()?>/img/3081627.jpg" alt="">
        </div>
        <div class="col-lg-5">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
            dolore
            magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
            commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur.</p>
        </div>
      </div>
      <div class="line"></div>
    </div>
  </section>