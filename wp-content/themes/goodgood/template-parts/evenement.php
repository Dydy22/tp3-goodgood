<?php
/*
Template Name: Evenement
Template Post Type: événements
*/
?>
<section>
    <div class="container">
      <h2>Événements</h2>
      <div class="row">

<?php
$eventloop = new WP_Query(
    array(
        'post_type' => 'événements',
        'posts_per_page' => 3
    )
);
while ( $eventloop->have_posts() ) : $eventloop->the_post();
?>

        <div class="col-lg-4">
            <?php the_post_thumbnail()?>
          <h3><?php the_title();?></h3>
          <p>32 septembre 2A21</p>
        </div>
      </div>

<?php endwhile;?>

      <div class="line"></div>
    </div>
  </section>