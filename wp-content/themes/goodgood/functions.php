<?php
/**
 * GoodGood functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package GoodGood
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'good_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function good_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on GoodGood, use a find and replace
		 * to change 'good' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'good', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'good' ),
				'main-menu' => esc_html__( 'Main', 'good' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'good_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'good_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function good_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'good_content_width', 640 );
}
add_action( 'after_setup_theme', 'good_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function good_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'good' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'good' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'good_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function good_scripts() {
	wp_enqueue_style( 'good-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'good-style', 'rtl', 'replace' );
	wp_enqueue_style('main', get_template_directory_uri() . '/css/main.css', array(), filemtime(get_template_directory() . '/css/main.css'), false);
	wp_enqueue_style('normalize', get_template_directory_uri() . '/css/normalize.css', array(), filemtime(get_template_directory() . '/css/normalize.css'), false);
	wp_enqueue_style('bootstrap-grid', get_template_directory_uri() . '/css/bootstrap-grid.css', array(), filemtime(get_template_directory() . '/css/bootstrap-grid.css'), false);

	wp_enqueue_script( 'good-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'good_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

// custom post type function
function create_posttype() {
    register_post_type( 'événements',
        array(
            'labels' => array(
                'name' => __( 'Événements' ),
                'singular_name' => __( 'Événements' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'evenements'),
			'show_in_rest' => true,
			'supports' => array( 'title', 'thumbnail', 'editor')
        )
	);

	register_post_type( 'équipe',
        array(
            'labels' => array(
                'name' => __( 'Équipe' ),
                'singular_name' => __( 'Équipe' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'equipe'),
			'show_in_rest' => true,
			'supports' => array( 'title', 'thumbnail',)
        )
	);
	
	register_post_type( 'intro',
        array(
            'labels' => array(
                'name' => __( 'Intro' ),
                'singular_name' => __( 'Intro' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'intro'),
			'show_in_rest' => true,
			'supports' => array( 'title', 'thumbnail', 'editor')
        )
    );
}
add_action( 'init', 'create_posttype' );

