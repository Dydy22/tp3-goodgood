<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package GoodGood
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'good' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="site-branding">
		
		<div class="container">	
			<nav id="site-navigation" class="main-navigation">
				<h1 class="logo">Good<span>Good</span></h1>
				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'good' ); ?></button>
				<div>
					<?php
					wp_nav_menu(
						array(
							'theme_location' => 'main-menu',
							'menu_id'        => 'main-menu',
							'menu_class' 	=> '',
						)
					);
					?>
					<button class="btn">Dark Mode</button>
				</div>
			</nav><!-- #site-navigation -->
		</div>
	</header><!-- #masthead -->
