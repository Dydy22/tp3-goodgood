<?php
get_header();
?>
 <div class="hero-banner">
      <h2 class="logo">Good<span>Good</span></h2>
      <h3>CRM</h3>
    </div>
<?php
get_template_part('template-parts/intro', get_post_type());
get_template_part('template-parts/evenement', get_post_type());
get_template_part('template-parts/equipe', get_post_type());
get_template_part('template-parts/contact', get_post_type());
get_footer();