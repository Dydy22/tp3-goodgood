<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_goodgood' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ',JPebcc[.@a^9c*$.WP^Ef3;qRI|x#63v6(:c7E@j]A.gIy0e [(OMaZGa8)R@~t' );
define( 'SECURE_AUTH_KEY',  ')Do~AUkGf!HECD|Ox.w<N]JRh@)!Zh>cRqoXsqB9(YT>z,;vJn5d_d4y.YchL5W8' );
define( 'LOGGED_IN_KEY',    'Lx04s6/Z`M%~*]_BJ6V$B*PA,Ise(CjNy)[LwVUi^2LNF%IA_Ca6%KTu*^W<PR),' );
define( 'NONCE_KEY',        '4u`GgF@c|)w$aK2v!$e{IdF7lb9.H%$b$!3[zB00$sEW!~gepQE;(v`xkY)6SsEY' );
define( 'AUTH_SALT',        ' [/7,m^F( n()!IGa)BR_KJNVqAnIvIUpJ;WkbLD?Nha|^qTP!,}o)M,45KbUe6I' );
define( 'SECURE_AUTH_SALT', ':L2{ok7#`NFBW>uWy>;K@awQ(LL6fEduinaMWb>1t7`i$9PqH&R;v+o,l.d0$CJf' );
define( 'LOGGED_IN_SALT',   '=&RuHyd`?KFNh7KXCTaW2Bp?|4r=4DP[B92M7/S-wgfU+reVO97FA21%)f5|<`(4' );
define( 'NONCE_SALT',       'DKw(r$Kze-av&Elz|3OYNMuI0BRvl@8hRV oOurEQ?@Q ,ysjxY*8wsD;$Y0tLbg' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
